import { Component } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent {
  // String Interpolation
  name="Lenin"
  nameIs(){
    return this.name
  }
  // Property Binding
  serverOn=true;
  // Event Binding
  result=''
  onSubmit(event:any  ){
      this.result=(<HTMLInputElement>event.target).value;
      // return this.result
  }
  // Two Way Binding
  isServer="Lenin"

}
