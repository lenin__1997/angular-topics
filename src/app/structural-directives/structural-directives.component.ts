import { Component } from '@angular/core';

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrls: ['./structural-directives.component.css']
})
export class StructuralDirectivesComponent {
  // Angular If
  users=true;
  // Angular For
  // letters=['A','B','C','D']
  letters = [{name:'Arun',age:24},
             {name:'Saravana',age:24},
             {name:'Rex',age:25}]
  // Angular Switch
  num=''
}
