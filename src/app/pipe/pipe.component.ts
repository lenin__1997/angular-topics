import { Component } from '@angular/core';

@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.css']
})
export class PipeComponent {
  title = 'This is the Way'
  handleEvent(){
    console.log('Button Clicked', this.title)
  }
  jsonValue={
    a:'hello',
    b:'world'
  }
  newDate = new Date();
}
